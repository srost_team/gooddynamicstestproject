//
//  AppDelegate.m
//  TestingGoodD
//
//  Created by Stanislav Razbegin on 9/7/15.
//  Copyright (c) 2015 Stanislav Razbegin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self setWindow:[[GDiOS sharedInstance] getWindow]];
    
    //Set a GDiOS instance and set the delegate
    [self setGood:[GDiOS sharedInstance]];
    [_good setDelegate:self];
    //Show the Good Authentication UI
    [_good authorize];
    _providers = [[NSMutableArray alloc] init];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Good Dynamics Delegate Methods

-(void)handleEvent:(GDAppEvent*)anEvent
{
    /* Called from _good when events occur, such as system startup. */
    
    switch (anEvent.type)
    {
        case GDAppEventAuthorized:
        {
            [self onAuthorized:anEvent];
            break;
        }
        case GDAppEventNotAuthorized:
        {
            [self onNotAuthorized:anEvent];
            break;
        }
        case GDAppEventRemoteSettingsUpdate:
        {
            // handle app config changes
            break;
        }
        case GDAppEventServicesUpdate:
        {
            NSLog(@"Received Service Update event");
            [self onServiceUpdate:anEvent];
            break;
        }
        case GDAppEventPolicyUpdate:
        {
            //A change to one or more application-specific policy settings has been received.
            break;
        }
        case GDAppEventEntitlementsUpdate:
        {
            //A change to the entitlements data has been received.
            break;
        }
        default:
        {
            NSLog(@"event not handled: %@", anEvent.message);
            break;
        }
    }
}

-(void) onNotAuthorized:(GDAppEvent*)anEvent
{
    /* Handle the Good Libraries not authorized event. */
    
    switch (anEvent.code) {
        case GDErrorActivationFailed:
        case GDErrorProvisioningFailed:
        case GDErrorPushConnectionTimeout:
        case GDErrorSecurityError:
        case GDErrorAppDenied:
        case GDErrorAppVersionNotEntitled:
        case GDErrorBlocked:
        case GDErrorWiped:
        case GDErrorRemoteLockout:
        case GDErrorPasswordChangeRequired: {
            // an condition has occured denying authorization, an application may wish to log these events
            NSLog(@"onNotAuthorized %@", anEvent.message);
            break;
        }
        case GDErrorIdleLockout: {
            // idle lockout is benign & informational
            break;
        }
        default:
            NSAssert(false, @"Unhandled not authorized event");
            break;
    }
}

-(void) onAuthorized:(GDAppEvent*)anEvent
{
    /* Handle the Good Libraries authorized event. */
    
    switch (anEvent.code) {
        case GDErrorNone: {
                // launch application UI here
                //started = YES;
                //RootViewController *rootViewController = nil;
                
                // set up a ServiceController
                //serviceController = [[ServiceController alloc] init];
                
                
                //Detect the device
                    /*
                     *	iPhone start - based on single UINavigationController
                     */
                    
                    //Setup the root view controller
                    UIViewController * rootViewController = [[UIViewController alloc] init];
                    
                    //Start up the root navigation controller with the root view controller
                    _navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
                    [self.window setRootViewController:_navController];

                
                
            
            break;
        }
        default:
            NSAssert(false, @"Authorized startup with an error");
            break;
    }
    
}

-(void)onServiceUpdate:(GDAppEvent*)anEvent{
    switch(anEvent.code){
        case GDErrorNone:{
            
            //Post change
           // [[NSNotificationCenter defaultCenter] postNotificationName:kServiceConfigDidChangeNotification object:nil];
            
        }break;
        default:
            NSAssert(false, @"Service update error");
            break;
    }
}


-(NSArray*) getProvidersForService:(NSString*)serviceId
{
    return [self.good getServiceProvidersFor:serviceId andVersion:nil andType:GDServiceProviderApplication];
}


@end
