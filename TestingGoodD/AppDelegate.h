//
//  AppDelegate.h
//  TestingGoodD
//
//  Created by Stanislav Razbegin on 9/7/15.
//  Copyright (c) 2015 Stanislav Razbegin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GD/GDiOS.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GDiOSDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GDiOS *good;
@property (strong, nonatomic) NSMutableArray *providers;
@property (strong, nonatomic) UINavigationController *navController;

-(void) onAuthorized:(GDAppEvent*)anEvent;
-(void) onNotAuthorized:(GDAppEvent*)anEvent;
-(void) onServiceUpdate:(GDAppEvent*)anEvent;

-(NSArray*) getProvidersForService:(NSString*)serviceId;

@end

